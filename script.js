document.getElementById('ipButton').addEventListener('click', async () => {
    try {
        const ipResponse = await fetch('https://api.ipify.org/?format=json');
        const { ip } = await ipResponse.json();

        const ipApiUrl = `http://ip-api.com/json/${ip}`;
        const ipApiResponse = await fetch(ipApiUrl);
        const ipInfo = await ipApiResponse.json();

        const infoDiv = document.getElementById('ipInfo');
        infoDiv.innerHTML = `
            <p>Континент: ${ipInfo.continent}</p>
            <p>Країна: ${ipInfo.country}</p>
            <p>Регіон: ${ipInfo.regionName}</p>
            <p>Місто: ${ipInfo.city}</p>
            <p>Район: ${ipInfo.district}</p>
        `;
    } catch (error) {
        console.error('Помилка при отриманні даних:', error);
    }
});