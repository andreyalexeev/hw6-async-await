Асинхронний JavaScript дозволяє створювати більш реактивні та продуктивні веб-додатки, зменшуючи час очікування та покращуючи користувацький досвід.
Дозволяє виконувати декілька завдань одночасно.
Використовується для завдань, які можуть займати багато часу (наприклад, отримання даних з сервера).
Використовується з Web API, такими як fetch, setTimeout, setInterval, Promise, async/await.